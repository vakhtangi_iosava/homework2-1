fun main() {

    val carfactory: CarFactory = Audi.Factory
    val car = carfactory.create("Audi")

    println(car?.drive())

    val factory: AircraftFactory = Boeing737.Factory
    val aircraft = factory.create("Boeing777")

    println(aircraft?.fly())
}